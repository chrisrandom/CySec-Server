from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from blogging.models import Blog, BlogPost


class BloggingTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="testuser", password="password")

    def test_blog_creation(self):
        """ test if we can create a blog for our user """
        url = reverse('blogging:create_blog')
        self.client.force_login(user=self.user)
        payload = {'title': 'Test Blog'}
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Blog.objects.count(), 1)

    def test_post_creation(self):
        """ test if we can create post for user """
        Blog.objects.create(title="Test Blog", user=self.user)
        self.client.force_login(user=self.user)
        url = reverse('blogging:create_post')
        payload = {'title': 'some post title', 'post': 'lorem ipsum'}
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(BlogPost.objects.count(), 1)

    def test_blog_deletion(self):
        """ ensure that we can delete blog"""
        blog = Blog.objects.create(title="Test Blog", user=self.user)
        self.client.force_login(user=self.user)
        url = reverse('blogging:delete_blog', kwargs={'blog_slug': blog.slug})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Blog.objects.count(), 0)

    def test_single_post_view(self):
        """ ensure we can view a single post """
        blog = Blog.objects.create(title="Test Blog", user=self.user)
        post = BlogPost.objects.create(
            title="Test Title", post="lorem ipsum", blog=blog)
        url = reverse(
            'blogging:single_post',
            kwargs={'post_slug': post.slug, 'blog_slug': blog.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_blog_view(self):
        """ ensure we can view a users blog """
        blog = Blog.objects.create(title="Test Blog", user=self.user)
        for itter in range(10):
            BlogPost.objects.create(
                title="Test Title %s" % itter,
                post="lorem ipsum %s" % itter,
                blog=blog)
        url = reverse('blogging:index', kwargs={'blog_slug': blog.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context.get('posts')), 10)