""" blogging views """
from django.views import generic
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from blogging.models import Blog, BlogPost
from blogging import forms

# Create your views here.

class BlogIndex(generic.ListView):
    template_name = "blogging/blog_index.html"
    context_object_name = "posts"
    paginate_by = 10

    def get_queryset(self):
        return BlogPost.objects.filter(blog__slug=self.kwargs.get('blog_slug'))

    def get_context_data(self, *args, **kwargs):
        context = super(BlogIndex, self).get_context_data(*args, **kwargs)
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug'))
        context.update({'blog': blog})
        return context



class CreateBlog(LoginRequiredMixin, generic.CreateView):
    template_name = "blogging/create_blog.html"
    form_class = forms.CreateBlogForm

    def form_valid(self, form):
        instance = form.instance
        instance.user = self.request.user
        instance.save()
        return super(CreateBlog, self).form_valid(form)



class CreatePost(LoginRequiredMixin, generic.CreateView):
    template_name = "blogging/create_post.html"
    form_class = forms.CreatePostForm


    def get_context_data(self, *args, **kwargs):
        context = super(CreatePost, self).get_context_data(*args, **kwargs)
        blog = get_object_or_404(Blog, user=self.request.user)
        context.update({'blog': blog})
        return context


    def form_valid(self, form):
        instance = form.instance
        blog = get_object_or_404(Blog, user=self.request.user)
        instance.blog = blog
        instance.save()
        return super(CreatePost, self).form_valid(form)



class SinglePost(generic.DetailView):
    template_name = "blogging/single_post.html"
    context_object_name = "post"
    slug_url_kwarg = 'post_slug'

    def get_queryset(self):
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug'))
        queryset = BlogPost.objects.filter(blog=blog, slug=self.kwargs.get('post_slug'))
        return queryset


class DeleteBlog(generic.DeleteView):
    template_name = "blogging/delete_blog.html"
    context_object_name = "blog"
    slug_url_kwarg = "blog_slug"


    def get_success_url(self):
        return reverse_lazy('accounts:user_profile', kwargs={'user_id': self.request.user.pk})


    def get_queryset(self):
        queryset = Blog.objects.filter(
            user=self.request.user, slug=self.kwargs.get('blog_slug'))
        return queryset
