from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator
from .models import BugBountyVulnerability, BugBountyDomain
from bug_bounty_mode import models

class NewVulnerabilityForm(forms.ModelForm):
    """ form for creating a new vulnerability """
    domain_name = forms.URLField(label='Domain', max_length=64)
    send_email_copy_to_user_check = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': 'checkbox form-check-input'}),
        label="Receive mail copy?", required=False)
    publish_vulnerability = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': 'checkbox form-check-input'}),
        label="Publish Vulnerability?", required=False, initial=True)

    class Meta:
        model = BugBountyVulnerability
        fields = (
            'domain_name', 'vuln_type', 'owner_contact_email',
            'path', 'parameter', 'poc',
            'description', 'send_email_copy_to_user_check', 'publish_vulnerability')
        help_texts = {
            'owner_contact_email': ('E-Mail address to contact domain owner'),
            'parameter': ('Vulnerable request parameter name'),
            'path': ('URL-Path where the vulnerability is located'),
            'poc': ('A Proof-of-Concept, that allows the domain admin to reproduce issue'),
            'description': ('optional: Some additional words, that describes the vulnerabiltiy'),
        }

    def save(self, commit=True):
        instance = super(NewVulnerabilityForm, self).save(commit=False)
        domain, _created = BugBountyDomain.objects.get_or_create(
            domain=self.cleaned_data['domain_name'])
        self.cleaned_data['domain'] = domain.pk
        instance.send_email_copy_to_user = self.cleaned_data['send_email_copy_to_user_check']
        instance.domain = domain
        if commit:
            instance.save()
        return instance


class ResendEmailForm(forms.Form):
    pass


class IncreaseDisclosureTimeForm(forms.Form):
    days = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'number'}),
        label="Additional Days", validators=[
            MaxValueValidator(14),
            MinValueValidator(1)
        ])


class NewCommentForm(forms.Form):
    comment = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '10'}),
        label="Comment")
    is_fixed = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': 'checkbox form-check-input'}),
        label="Fixed?", required=False)


class CreateVulnTypeForm(forms.ModelForm):
    class Meta:
        model = models.VulnerabilityType
        fields = (
            'name', 'description',
            'how_to_fix', 'consequences')

    def __init__(self, *args, **kwargs):
        super(CreateVulnTypeForm, self).__init__(*args, **kwargs)
        for _key, field in self.fields.items():
            field.widget.attrs.update({'class':'form-control'})