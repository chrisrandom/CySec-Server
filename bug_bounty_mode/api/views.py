from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from bug_bounty_mode.api import serializers
from bug_bounty_mode import models


class CreateDomain(generics.CreateAPIView):
    """ create a new domain """
    serializer_class = serializers.DomainSerializer
    permission_classes = [IsAuthenticated]


class CreateBug(generics.CreateAPIView):
    """ create a new bug """
    serializer_class = serializers.BugSerializer
    permission_classes = [IsAuthenticated]


class CreateVulnerabilityType(generics.CreateAPIView):
    """ create a new vulnerability type """
    serializer_class = serializers.VulnerabilityTypeSerializer
    permission_classes = [IsAuthenticated]


class ListMyVulnerabilities(generics.ListAPIView):
    """ outputs all vulnerabilities published by this user """
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.BugSerializer

    def get_queryset(self):
        return models.BugBountyVulnerability.objects.filter(user=self.request.user)
