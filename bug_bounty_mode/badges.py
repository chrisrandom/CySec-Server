from pinax.badges.base import Badge, BadgeAwarded, BadgeDetail

from bug_bounty_mode import models


class SecuredWebsitesBadge(Badge):
    slug = "Secured Websites"
    levels = [
        BadgeDetail(name="Bronze", description="+10 Websites"),
        BadgeDetail(name="Silver", description="+100 Websites"),
        BadgeDetail(name="Gold", description="+500 Websites"),
        BadgeDetail(name="Master", description="+1000 Websites")
    ]
    events = ["secured_websites_awarded"]
    multiple = False

    def award(self, **state):
        user = state.get('user')
        bugs = models.BugBountyVulnerability.objects.filter(user=user, send_to_owner=True).count()
        if bugs > 1000:
            return BadgeAwarded(level=4)
        elif bugs > 500:
            return BadgeAwarded(level=3)
        elif bugs > 100:
            return BadgeAwarded(level=2)
        elif bugs > 10:
            return BadgeAwarded(level=1)


class PatchedBadge(Badge):
    slug = "Patch"
    levels = [
        BadgeDetail(name="Bronze", description="20% patched"),
        BadgeDetail(name="Silver", description="50% patched"),
        BadgeDetail(name="Gold", description="80% patched"),
        BadgeDetail(name="Master", description="100% patched")
    ]
    events = ["patched_websites_awarded"]
    multiple = False

    def award(self, **state):
        user = state.get('user')
        bugs = models.BugBountyVulnerability.objects.filter(user=user, send_to_owner=True)
        all_bugs = bugs.count()
        fixed_bugs = bugs.filter(fixed=True).count()
        percent = 100 * fixed_bugs / all_bugs
        if percent >= 100:
            return BadgeAwarded(level=4)
        elif percent >= 80:
            return BadgeAwarded(level=3)
        elif percent >= 50:
            return BadgeAwarded(level=2)
        elif percent >= 20:
            return BadgeAwarded(level=1)
