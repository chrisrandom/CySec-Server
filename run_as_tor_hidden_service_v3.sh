DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
echo $DIR

echo "Build cysec server container..."
docker build -t cysecserver .
echo "Starting cysec server container..."
docker run --name cysecserver -d -v $DIR/srv/static:/cysec_server/srv/static -v $DIR/local_settings.py:/cysec_server/local_settings.py cysecserver

echo "Starting nginx container..."
docker run --link cysecserver --name nginx -v $DIR/srv/static:/cysec_server/static -v $DIR/utils/docker/nginx/nginx.conf:/etc/nginx/conf.d/default.conf:ro -d nginx:alpine

echo "Building hidden service ..."
docker build -t tor-hs-v3 ./utils/docker/docker-hidden-service-v3
echo "Starting hidden service..."
docker run --name hiddenservice1 -v $DIR/srv/hidden_service1:/var/lib/tor/hidden_service -ti --link nginx -d tor-hs-v3
echo "Starting hidden service clone..."
docker run --name hiddenservice2 -v $DIR/srv/hidden_service2:/var/lib/tor/hidden_service -ti --link nginx -d tor-hs-v3


echo "Your hiddenservice 1: "
docker exec -it hiddenservice1 /bin/sh -c 'cat /var/lib/tor/hidden_service/nginx/hostname'

echo "Your hiddenservice 2:"
docker exec -it hiddenservice2 /bin/sh -c 'cat /var/lib/tor/hidden_service/nginx/hostname'