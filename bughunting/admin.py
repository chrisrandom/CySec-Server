from django.contrib import admin
from bughunting import models
# Register your models here.


admin.site.register(models.VulnerabilityType)
admin.site.register(models.Domain)
admin.site.register(models.Vulnerability)
