from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
import uuid
from pinax.badges.registry import badges
from bughunting import badges as my_badges
# Create your models here.


class VulnerabilityType(models.Model):
    name = models.CharField(unique=True, max_length=128)
    slug = models.SlugField()
    description = models.TextField()
    how_to_fix = models.TextField()
    consequences = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    updated_from = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="updated_from")

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name)
        return super(VulnerabilityType, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Type"
        verbose_name_plural = "Types"


class Domain(models.Model):
    domain = models.URLField(unique=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.domain

    class Meta:
        verbose_name_plural = "Domains"
        verbose_name = "Domain"


class Vulnerability(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True)
    vuln_type = models.ForeignKey(VulnerabilityType, on_delete=models.CASCADE)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="bug")
    parameter = models.CharField(max_length=64)
    path = models.CharField(max_length=128)
    proof_of_concept = models.TextField()
    is_fixed = models.BooleanField(default=False)
    fixed_at = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    disclose_deadline = models.DateField(blank=True, null=True)
    is_disclosed = models.BooleanField(default=False)
    disclosed_at = models.DateField(blank=True, null=True)
    target_email = models.EmailField()
    description = models.TextField(blank=True, null=True)
    is_send = models.BooleanField(default=False)
    send_at = models.DateTimeField(blank=True, null=True)
    send_email_copy = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Vulnerability"
        verbose_name_plural = "Vulnerabilities"
        unique_together = ('domain', 'parameter', 'path', 'vuln_type')
        ordering = ('-created_at',)


# Register Badges here
badges.register(my_badges.SecuredWebsitesBadge)
badges.register(my_badges.PatchedBadge)