from django.urls import path
from django.contrib.auth.views import PasswordResetCompleteView
from django.contrib.auth.views import PasswordResetDoneView
from django.contrib.auth.views import LogoutView
from accounts import views

app_name = "accounts"

urlpatterns = [

    path(
        'login/', views.AccountLoginView.as_view(), name='login_view'),


    # logout user
    path(
        'logout/',
        LogoutView.as_view(next_page='accounts:login_view'),
        name="logout_view"),


    # change password view
    path(
        'password/change',
        views.ChangePasswordView.as_view(), name='change_password'),


    # signup view
    path(
        'signup/', views.SignUpView.as_view(), name='signup_view'),


    # activate account view (check activation tokens)
    path(
        'activate/<str:uidb64>/<str:token>/',
        views.ActivateAccountView.as_view(), name='activate_account_view'),


    # show form for resetting password
    path(
        'lost_password/',
        views.PasswordResetView.as_view(
            template_name='accounts/password_reset.html'),
        name='reset_password'),


    path(
        'lost_password/confirm/<str:uidb64>/<str:token>',
        views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),


    path(
        'lost_password/done',
        PasswordResetDoneView.as_view(
            template_name='accounts/registration/password_reset_done.html'),
        name='password_reset_done'),


    path(
        'lost_password/complete',
        PasswordResetCompleteView.as_view(
            template_name="accounts/registration/password_reset_complete.html"),
        name='password_reset_complete'),


    path('', views.Index.as_view(), name="index"),


    path('profile/user/<str:user_id>', views.ShowUserProfile.as_view(), name='user_profile'),


    path(
        'profile/user/<str:user_id>/found/vulns',
        views.ListVulnsFoundByUser.as_view(), name='user_found_vulns'),


    path(
        'profile/refresh_api_token',
        views.RefreshAPIToken.as_view(),
        name="refresh_api_token"),


    path(
        'profile/change_email',
        views.ChangeEMailAddressView.as_view(),
        name="change_email_address"),


    path(
        'profile/change_email/confirm/<str:uidb64>/<str:token>',
        views.ValidateChangeEMailAddressView.as_view(),
        name="confirm_change_email_address"),


    path(
        'profile/settings', views.Settings.as_view(),
        name="settings"),

    path('dashboard', views.Dashboard.as_view(), name="dashboard")
]
