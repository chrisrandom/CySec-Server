from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from django.shortcuts import get_object_or_404
from accounts.api import serializers
from accounts import models


class RefreshAPIToken(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.RefreshAPITokenSerializer

    
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        if not queryset.exists():
            Token.objects.create(user=self.request.user)
            queryset = self.filter_queryset(self.get_queryset())
        obj = get_object_or_404(queryset)
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj

    def get_queryset(self):
        return Token.objects.filter(user=self.request.user)



class UpdateEnablePublicMailbox(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.UpdateEnablePublicMailboxSerializer

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = get_object_or_404(queryset)
        self.check_object_permissions(self.request, obj)
        return obj
    
    def get_queryset(self):
        return models.UserProfile.objects.filter(user=self.request.user)



class UsersMailboxPublicKey(generics.RetrieveAPIView):
    serializer_class = serializers.MailboxPublicKeySerializer

    def get_queryset(self):
        return models.UserProfile.objects.filter(user=self.kwargs.get('pk'), public_key__isnull=False)