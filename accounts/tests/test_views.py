""" module for testing social engineering views """
from django.test import TestCase, override_settings
from django.contrib.auth.models import User
from django.urls import reverse
from django.core import mail
from pentesting.models import Project
from pentesting import models


class AccountViewsTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="testuser")
        self.user.set_password("password")
        self.user.save()
        self.project = Project.objects.create(name="Test Project")
        models.UserProject.objects.create(user=self.user, project=self.project, role="owner")
        self.client.login(username="testuser", password="password")
        session = self.client.session
        session['active_project'] = str(self.project.pk)
        session.save()

    @override_settings(REGISTRATION_ENABLED=True)
    def test_signup(self):
        self.client.logout()
        url = reverse('accounts:signup_view')
        payload = {
            'username': 'testuser2', 'password1': 'test-1-1-1',
            'password2': 'test-1-1-1', 'email': 'asdf2@example.com'}
        response = self.client.post(url, payload, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(len(mail.outbox), 1)

    @override_settings(REGISTRATION_ENABLED=False)
    def test_signup_flawed(self):
        self.client.logout()
        url = reverse('accounts:signup_view')
        payload = {
            'username': 'hacker', 'password1': 'test-1-1-1',
            'password': 'test-1-1-1', 'email': 'hacker@example.com'
        }
        response = self.client.post(url, payload, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 0)

    def test_login(self):
        self.client.logout()
        url = reverse('accounts:login_view')
        payload = {
            'username': self.user.username,
            'password': 'password'
        }
        response = self.client.post(url, payload, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.context.get('user')), self.user.username)

    def test_activate_account(self):
        # TODO: implement
        pass

    def test_validate_change_email_address(self):
        # TODO: implement
        pass

    def test_change_email_address(self):
        url = reverse('accounts:change_email_address')
        payload = {'email': 'new@example.com'}
        response = self.client.post(url, payload, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.profile.email_confirmed, False)
        self.assertEqual(len(mail.outbox), 1)
