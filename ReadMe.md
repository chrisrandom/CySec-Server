# Description

This is the server part of the CySec-Framework. The server provides a webinterface 
for easily managing your pentestings or bug bounty vulnerabilities. 


*This is still beta and may contain lot of bugs and only basic UX for now.*

I plan to include other features in future like a client application ;)

Please report bugs and other ideas through the [Bug-Tracker](https://codeberg.org/cysec/CySec-Server/issues)


# Setup
See wiki pages for setup instructions

# Features

- Pentesting
    - Unlimited amount of pentesting projects
    - Add other users to your project and specify a role like "pentester" or "project admin"
    - Tasks that supports Markdown. Tasks can optionally be assigned to a user
    - create pentesting reports based on the discovered information and vulnerabilities
    - optionally encrypt reports using AES-GCM or ChaCha20Poly1305 (server-side)
    - Dashboard with latest activities and statistics


- Bug Bounty
    - Bug Hunters can win awards for securing websites
    - diclose vulnerabilities within 30 days
    - bug hunter can increaase dislosure deadline
    - store discovered bugs as draft
    - optionally submit social media posts, if vulnerability was submitted (only mastodon is supported for now)
    - show bug details only using sharable link, which prevent domain administration to create accounts on the server
    - details of not disclosed or fixed vulnerabilities are hidden for other users
    - sending email to site admins for vulnerability notification


- Landing Page
    - Customize Carousel images and text
    - displays statistical stuff using charts (i.e.: Top Bug Hunters, Top Vulnerabilities,...)

- Public user profile pages with awards included

- Public Profile Page with user stats

- enable or disable registration

- REST-API allows external tools to interact with our service

- Users can create blogs ( with markdown support )

- Encrypted mailboxes for users ( using PGP )


there are more features planned. see issues page.
