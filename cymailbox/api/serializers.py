from rest_framework import serializers
from cymailbox import models

class MarkMessageAsReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Mailbox
        fields = ('read', 'pk')
        read_only_fields = ('pk',)


class MessageDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Mailbox
        fields = ('message', 'subject')
        read_only_fields = ('message', 'subject')