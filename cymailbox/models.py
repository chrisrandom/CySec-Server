import uuid
from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Mailbox(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subject = models.CharField(max_length=124)
    message = models.TextField()
    sender_email_address = models.EmailField()
    send_at = models.DateTimeField(auto_now_add=True)
    read = models.BooleanField(default=False)

    class Meta:
        ordering = ('-send_at',)
