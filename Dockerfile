FROM frolvlad/alpine-python3

WORKDIR /cysec_server
ADD . /cysec_server
ADD ./utils/docker/start_gunicorn.sh /start_gunicorn.sh
RUN chmod +x /start_gunicorn.sh

RUN apk update && apk add gcc musl-dev postgresql-dev dcron libffi-dev zlib-dev jpeg-dev \
python3-dev make
#texlive texmf-dist-latexextra

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN chmod +x /cysec_server/disclose_vulnerabilities.py
RUN echo '* * * * * python3 /cysec_server/disclose_vulnerabilities.py >> /bbv_disclosure.log 2>&1 \n' > /var/spool/cron/crontabs/root

ENTRYPOINT [ "sh", "/start_gunicorn.sh" ]