from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token

app_name = "api"

urlpatterns = [
    path('pentesting/', include('pentesting.api.urls')),
    path('bug_bounty_mode/', include('bug_bounty_mode.api.urls')),
    path('blogs/', include('blogging.api.urls')),
    path('stats/', include('stats.api.urls')),
    path('accounts/', include('accounts.api.urls')),
    path('mailbox/', include('cymailbox.api.urls')),
    path('token-auth', obtain_auth_token)
]
