"""
Template for local settings
"""
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


ALLOWED_HOSTS = ["10.0.0.5"]

# Allow users to create a new account on the server
REGISTRATION_ENABLED = False

# Configure email settings
EMAIL_HOST = 'smtp.riseup.net'
EMAIL_PORT = 587
EMAIL_HOST_USER = "john.doe"
EMAIL_HOST_PASSWORD = "averylongandsecurepassword"
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
DEFAULT_FROM_EMAIL = "john.doe@riseup.net"


# -----------------------------
# Configure Bug-Bounty Social-Media Clients.
# Social-Media Clients will publish posts or messages
# if one of your bug hunters published a bug bounty vulnerability
# -----------------------------
MASTODON_ENABLED = False
MASTODON_URL = "https://mastodon.social"
MASTODON_EMAIL = "john.doe@riseup.net"
MASTODON_PASSWORD = "anotherverylongandsecurepassword"
# Message, you want to pulbish. Use placeholder here
MASTODON_TOOT_MESSAGE = """
We just published a {{VULNERABILITY}} found by one of our bug hunters. Vendor contacted! More information: {{LINK}}
#cysecframework
"""

# E-Mail subject for signup account activation
SIGNUP_ACTIVATION_EMAIL_SUBJECT = "Activate your CySec Account"

# used for bug bounty access token too. How long are created token valid
PASSWORD_RESET_TIMEOUT_DAYS = 30

# -----------------------------
# Configure database settings
# Default: Uses SQLITE3
# -----------------------------

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql_psycopg2',
#        'NAME': 'db_name',                      
#        'USER': 'db_user',
#        'PASSWORD': 'db_user_password',
#        'HOST': '',
#        'PORT': 'db_port_number',
#    }
#}

REPORTS_HTML_TEMPLATES = [
    ('reports/default.html', 'Default HTML-Template')
]

# put these image inside the static/public directory
PUBLIC_CAROUSEL_IMAGES = [
    {'image': '1.jpg', 'header': 'Test Header 1', 'content': 'You can customize your cysec server easily'},
    {'image': '2.jpg', 'header': 'Test Header 2', 'content': 'This is just a placeholder'}
]
# -----------------------------
# Privacy: Configure proxy for sending emails
# -----------------------------
import socks
import smtplib
socks.setdefaultproxy(socks.SOCKS5, 'localhost', 9050)
socks.wrapmodule(smtplib)
