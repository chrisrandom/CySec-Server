from rest_framework import serializers
from bug_bounty_mode.models import BugBountyVulnerability

class LatestUserBugSerializer(serializers.ModelSerializer):
    month = serializers.DateField()
    count = serializers.IntegerField()

    class Meta:
        model = BugBountyVulnerability
        fields = ('count', 'month')