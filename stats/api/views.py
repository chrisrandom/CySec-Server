from rest_framework import generics
from rest_framework.response import Response
from django.db.models import Count
from django.contrib.auth.models import User
from bug_bounty_mode.models import BugBountyVulnerability, BugBountyDomain, VulnerabilityType


class TopBugBountyVulnerabilities(generics.RetrieveAPIView):

    def retrieve(self, _request, *args, **kwargs):
        queryset = VulnerabilityType.objects.filter(
            bugbountyvulnerability__send_to_owner=True).values('name').annotate(
                count=Count('bugbountyvulnerability__pk')).values('count', 'name')
        result = {
            'total': BugBountyVulnerability.objects.filter(send_to_owner=True).count(),
            'result': queryset
        }
        return Response(result)


class TopUsersByBugBountyVulnerabilities(generics.RetrieveAPIView):

    def get_queryset(self):
        queryset = User.objects.filter(
            bugbountyvulnerability__send_to_owner=True).annotate(
            count=Count('bugbountyvulnerability__pk')).values('count', 'username').order_by('-count')[:10]
        return queryset

    def retrieve(self, _request, *args, **kwargs):
        queryset = User.objects.filter(
            bugbountyvulnerability__send_to_owner=True).annotate(
                count=Count('bugbountyvulnerability__pk')).values('count', 'username').order_by('-count')[:10]
        return Response(queryset)


class LatestBugBountyVulnerabilitiesPerDay(generics.RetrieveAPIView):

    def get_queryset(self):
        queryset = BugBountyVulnerability.objects.extra(
            {'published': "date(created_at)"}).values(
            'published').order_by('-published').annotate(count=Count('pk'))
        queryset = sorted(queryset, key=lambda k: k['published'])[:15]
        return queryset

    def retrieve(self, _request, *args, **kwargs):
        queryset = BugBountyVulnerability.objects.extra(
            {'published':"date(created_at)"}).values(
                'published').order_by('-published').annotate(count=Count('pk'))
        queryset = sorted(queryset, key= lambda k: k['published'])[:15]
        return Response(queryset)


class MostVulnerableDomains(generics.RetrieveAPIView):

    def get_queryset(self):
        queryset = BugBountyDomain.objects.filter(
            bugbountyvulnerability__send_to_owner=True, bugbountyvulnerability__fixed=False).annotate(
            count=Count('bugbountyvulnerability__pk')).values('count', 'domain').order_by('-count')[:10]
        return queryset

    def retrieve(self, _request, *args, **kwargs):
        queryset = BugBountyDomain.objects.filter(
            bugbountyvulnerability__send_to_owner=True, bugbountyvulnerability__fixed=False).annotate(
                count=Count('bugbountyvulnerability__pk')).values('count', 'domain').order_by('-count')[:10]
        return Response(queryset)