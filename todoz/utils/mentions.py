import re
from django.contrib.auth.models import User
from notifications.signals import notify
from todoz.models import Task, Comment

USER_REGEX = r"(?<!\w)@(\w+)"

def notify_mentioned_users(instance):
    if isinstance(instance, Task):
        found_users = re.findall(USER_REGEX, instance.description)
        users = User.objects.filter(username__in=found_users)
        for user in users:
            notify.send(
                instance.creator, recipient=user,
                verb='mentioned you in task', action_object=instance, public=False)
    elif isinstance(instance, Comment):
        found_users = re.findall(USER_REGEX, instance.text)
        users = User.objects.filter(username__in=found_users)
        for user in users:
            notify.send(
                instance.user, recipient=user,
                verb='mentioned you in a task comment', action_object=instance, public=False)
